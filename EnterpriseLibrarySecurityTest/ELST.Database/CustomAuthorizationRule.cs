﻿using Microsoft.Practices.EnterpriseLibrary.Security;

namespace ELST.Database
{
	public class CustomAuthorizationRule : IAuthorizationRule
	{
		public string Name { get; private set; }

		public string Expression { get; private set; }
	}
}

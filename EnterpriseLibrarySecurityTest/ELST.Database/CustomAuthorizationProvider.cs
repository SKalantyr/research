﻿using System;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Security;

namespace ELST.Database
{
	public class CustomAuthorizationProvider : IAuthorizationProvider
	{
		public bool Authorize(IPrincipal principal, string context)
		{
			if (principal == null) throw new ArgumentNullException("principal");

			if (!principal.Identity.IsAuthenticated)
				return false;

			throw new NotImplementedException();
		}
	}
}

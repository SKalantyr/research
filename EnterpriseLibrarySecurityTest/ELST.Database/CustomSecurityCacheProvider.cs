﻿using System;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Security;

namespace ELST.Database
{
	public class CustomSecurityCacheProvider : ISecurityCacheProvider
	{
		public IToken SaveIdentity(IIdentity identity)
		{
			throw new NotImplementedException();
		}

		public void SaveIdentity(IIdentity identity, IToken token)
		{
			throw new NotImplementedException();
		}

		public IToken SavePrincipal(IPrincipal principal)
		{
			throw new NotImplementedException();
		}

		public void SavePrincipal(IPrincipal principal, IToken token)
		{
			throw new NotImplementedException();
		}

		public IToken SaveProfile(object profile)
		{
			throw new NotImplementedException();
		}

		public void SaveProfile(object profile, IToken token)
		{
			throw new NotImplementedException();
		}

		public void ExpireIdentity(IToken token)
		{
			throw new NotImplementedException();
		}

		public void ExpirePrincipal(IToken token)
		{
			throw new NotImplementedException();
		}

		public void ExpireProfile(IToken token)
		{
			throw new NotImplementedException();
		}

		public IIdentity GetIdentity(IToken token)
		{
			throw new NotImplementedException();
		}

		public IPrincipal GetPrincipal(IToken token)
		{
			throw new NotImplementedException();
		}

		public object GetProfile(IToken token)
		{
			throw new NotImplementedException();
		}
	}
}
